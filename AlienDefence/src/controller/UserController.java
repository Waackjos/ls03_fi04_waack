package controller;

import model.User;
import java.util.Date;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}
	//INSERT INTO `users` (`P_user_id`, `first_name`, `sur_name`, `birthday`,
			//`street`, `house_number`, `postal_code`, `city`, `login_name`, `password`,
			//`salary_expectations`, `marital_status`, `final_grade`)
	
	public User createUser(User user) {
		user = new User(1 ,"firstname", "name", Date(1900, 1, 1) , "street", "1", "10000", "city", "loginname", "pass", 10, "maritalstatus", 1.0);
		int user_id = 1;
		int user_id = userPersistance.createUser(user);
		
		return user;
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		return null;
	}
	
	public void changeUser(User user) {
		
	}
	
	public void deleteUser(User user) {
		
	}
	
	public boolean checkPassword(String username, String passwort) {
		return false;
	}
}
